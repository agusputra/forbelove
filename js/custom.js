$(function () {
    var owl;
    owl = $("#owl-custom");

    owl.owlCarousel({

        navigation: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        afterInit: afterOWLinit,
        afterUpdate : afterOWLinit,
        autoHeight: true

    });

    function afterOWLinit() {

        $('.owl-controls .owl-page').append('<a class="item-link" href="javascript:;"/>');

        var pafinatorsLink = $('.owl-controls .item-link');

        $.each(this.owl.userItems, function (i) {

            $(pafinatorsLink[i])
                .css({
                    'background': 'url(' + $(this).find('img.img-review').attr('src') + ') no-repeat center bottom',
                    '-webkit-background-size': '50%',
                    '-moz-background-size': '50%',
                    '-o-background-size': '50%',
                    'background-size': '70%'
                })

                .click(function () {
                    owl.trigger('owl.goTo', i);
                });

        });

        $('.owl-carousel').append('<a href="#prev" class="prev-owl"/>');
        $('.owl-carousel').append('<a href="#next" class="next-owl"/>');

        $(".next-owl").click(function () {
            owl.trigger('owl.next');
        });

        $(".prev-owl").click(function () {
            owl.trigger('owl.prev');
        });

    }

    //guide click
    $('.control-btn').on('click', function() {
        $.scrollTo($(this).closest('section').next(), {
            axis : 'y',
            duration : 500
        });
        return false;
    });

});